wget https://nodejs.org/en/download/
tar xvzf node*.tar.gz
rm -rf node*.tar.gz
sudo mkdir /opt/node
sudo cp -a node*/* /opt/node
 
sudo -i
echo "PATH=$PATH:/opt/node/bin" >> /root/.profile
echo "export PATH" >> /root/.profile
PATH=$PATH:/opt/node/bin
export PATH
exit
 
sudo echo "PATH=$PATH:/opt/node/bin" >> /home/pi/.profile
sudo echo "export PATH" >> /home/pi/.profile
PATH=$PATH:/opt/node/bin
export PATH

Если вдруг что-то пошло не так

sudo apt-get install libstdc++6

sudo add-apt-repository ppa:ubuntu-toolchain-r/test 
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade





//node-opencv

1. Delete all from system
sudo apt-get remove --purge node nodejs npm node-gyp node-pre-gyp
sudo apt-get autoremove
2. Download and install NVM (Node version manager)
I have notice that some times ubuntu have some troubles with nvm when you install things in root mode. Unroot if needed.
// check for newest version in git repo
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.7/install.sh | bash
// Update current terminal
source ~/.bashrc
// Check nvm version command
nvm -v
// Check current nvm node versions (must be empty list)
nvm ls
3. Install nodejs
// install nodejs needed version (must be > 0.10 for node-opencv i recommend 6.5+)
nvm install v6.5.0
// set default node version
nvm alias default v6.5.0
// check for instalation 6.5
nvm ls
(NPM installs automatic with nvm)
4. Opencv
// install opencv ubuntu library if needed
sudo apt-get install libopencv-dev
npm -g install node-gyp
npm -g install node-pre-gyp
npm -g install opencv
// or you can install it local and add to package.json //// npm --save install opencv
also recommend install nodemon for developement and supervizor for production