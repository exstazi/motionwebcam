const SerialPort = require("serialport");

const COMMAND_ON_220 = 1;
const COMMAND_OFF_220 = 2;
const COMMAND_ON_PLINTH = 3;
const COMMAND_OFF_PLINTH = 4;
const COMMAND_MANUAL_PLINTH = 5;
const COMMAND_ON_RGB = 6;
const COMMAND_OFF_RGB = 7;
const COMMAND_MANUAL_R = 8;
const COMMAND_MANUAL_G = 9;
const COMMAND_MANUAL_B = 10;

class LedDriver 
{
    constructor(opt)
    {
        this.update(opt);

        this.port = new SerialPort("COM5");
    }

    update(opt) 
    {
        opt = opt || {}

        this["delay-on-220"] = opt["on-220"] || 0;
        this["delay-off-220"] = opt["off-220"] || 0;
        this["delay-on-plinth"] = opt["on-plinth"] || 0;
        this["speed-on-plinth"] = opt["speed-on-plinth"] || 100;
        this["delay-off-plinth"] = opt["off-plinth"] || 0;
        this["speed-off-plinth"] = opt["speed-off-plinth"] || 100;
        this["delay-on-rgb"] = opt["on-rgb"] || 0;
        this["speed-change-rgb"] = opt["speed-change-rgb"] || 100;
        this["delay-off-rgb"] = opt["off-rgb"] || 0;
    }

    manual(opt)
    {
        if (opt["220"]) {
            this._write(COMMAND_ON_220);
        } else {
            this._write(COMMAND_OFF_220);
        }

        let plinth = opt["plinth"] || 0;
        let r = opt["r"] || 0;
        let g = opt["g"] || 0;
        let b = opt["b"] || 0;

        this._write(COMMAND_MANUAL_PLINTH, plinth);
        this._write(COMMAND_MANUAL_R, r);
        this._write(COMMAND_MANUAL_G, g);
        this._write(COMMAND_MANUAL_B, b);
    }

    start()
    {
        clearTimeout(this["timer-delay-off-220"]);
        clearTimeout(this["timer-delay-off-plinth"]);
        clearTimeout(this["timer-delay-off-rgb"]);

        this._execute("delay-on-220", COMMAND_ON_220);
        this._execute("delay-on-plinth", COMMAND_ON_PLINTH, this["speed-on-plinth"]);
        this._execute("delay-on-rgb", COMMAND_ON_RGB, this["speed-change-rgb"]);
    }

    stop()
    {
        if (this["timer-delay-on-220"] == 0) {
            this._execute("delay-off-220", COMMAND_OFF_220);
        } else {
            clearTimeout(this["timer-delay-on-220"]);
        }

        if (this["timer-delay-on-plinth"] == 0) {
            this._execute("delay-off-plinth", COMMAND_OFF_PLINTH), this["speed-off-plinth"];
        } else {
            clearTimeout(this["timer-delay-on-plinth"]);
        }

        if (this["timer-delay-on-rgb"] == 0) {
            this._execute("delay-off-rgb", COMMAND_OFF_RGB);
        } else {
            clearTimeout(this["timer-delay-on-rgb"]);
        }
    }

    _execute(prop, command, data = 0x00)
    {
        if (this[prop] > 0) {
            this["timer-" + prop] = setTimeout(() => {
                this._write(command, data);
                this["timer-" + prop] = 0;
            }, this[prop] * 1000);
        } else {
            this._write(command, data);
        }
    }

    _write(command, data = 0x00)
    {
        let buffer = Buffer.alloc(3, 0);
        buffer[0] = 0xFF;
        buffer[1] = command;
        buffer[2] = data;

        this.port.write(buffer);

        console.log(buffer);
    }
}





module.exports = LedDriver