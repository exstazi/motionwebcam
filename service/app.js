const Motion = require('./motion');
const LedDriver = require('./led_driver');
const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

const index = fs.readFileSync('./index.html', 'utf-8');
const config = require('./config.json');

const detect = new Motion(config.webcam);
const driver = new LedDriver(config.main);

if (config.state) {
    detect.start();
}

let prev_detect = undefined;

detect.on("detect", (val) => {
    if (val != prev_detect) {
        if (val) {
            driver.start();
        } else {
            driver.stop();
        }
    }
    prev_detect = val;

    console.log(val);
});

const app = express();

let updateConfig = function() {
    fs.writeFileSync('./config.json', JSON.stringify(config, null, 4));
    detect.update(config.webcam);
    driver.update(config.main);
}

app.use(express.static('assets'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.get('/', (req, res) => { res.send(index) });
app.route('/system')
    .post((req, res) => {
        if (req.body.command) {
            config.state = 1;
            detect.start();
        } else {
            config.state = 0;
            detect.stop();
            driver.stop();
        }

        updateConfig();
        res.json({state: 'ok'});
    });
app.route('/config')
    .get((req, res) => res.json(config))
    .put((req, res) => {
        Object.assign(config, req.body);

        updateConfig();
        res.json({state: 'ok'});
    });
app.route('/manual')
    .put((req, res) => {
        Object.assign(config, req.body);

        if (config.state == 0) {
            driver.manual(config.manual);
        }

        res.json({state: 'ok'});
    })

app.listen(80, function () {
    console.log('Example app listening on port 80!');
});