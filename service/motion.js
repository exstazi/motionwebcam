const cv = require("opencv");
const EventEmitter = require("events").EventEmitter;

const MOTION_THRESHOLD = 50;
const AREA_CONTOURS = 1200;
const FPS = 10;
const FRAME_WIDTH = 640;
const FRAME_HEIGHT = 480;

/**
 * @param {Object} opt
 * @param {Number} opt.threshold порог для бинаризации изображения
 * @param {Number} opt.contours_area площадь контура движенияъ
 * @param {Number} opt.motion_frame_count_on количество кадров, после которых считается что движение зафиксировано
 * @param {Number} opt.motion_frame_count_off количество кадров, после которых считается что движений нет
 * @param {Number} opt.fps 
 *  
 * @event detect true - зафиксировано движение, false - движение прекращено
 */
class Motion extends EventEmitter
{
	constructor(opt) {
		super();

		this.update(opt);
		
		this.prev_frame = cv.Matrix.Zeros(FRAME_HEIGHT, FRAME_WIDTH, cv.Constants.CV_BGR2GRAY);
		this.filters_frame = cv.Matrix.Zeros(FRAME_HEIGHT, FRAME_WIDTH, cv.Constants.CV_BGR2GRAY);

		this._worker_timer = 0;
		this._camera = null;
		this.current_motion_frame_on = 0;
		this.current_motion_frame_off = 0;
	}

	/**
	 * Обновить конфигурацию
	 * @param {Object} opt 
	 */
	update(opt) {
		opt = opt || {}

		this.threshold = opt.threshold || MOTION_THRESHOLD;
		this.contours_area = opt.contours_area || AREA_CONTOURS;
		this.motion_frame_count_on = opt.motion_frame_count_on || 1;
		this.motion_frame_count_off = opt.motion_frame_count_off || 1;
		this.fps = opt.fps || FPS;
		this.debug = opt.debug || false;

		if (this._worker_timer) {
			clearInterval(this._worker_timer);
			this._worker_timer = setInterval(() => this._worker(), 1000 / this.fps);
		}
	}

	start() {
		if (!this._worker_timer) {
			this._camera = new cv.VideoCapture(0);

			this._worker_timer = setInterval(() => this._worker(), 1000 / this.fps);

			if (this.debug) {
				this.window = new cv.NamedWindow('Video', 0)
			}
		}
	}

	stop() {
		if (this._worker_timer) {
			clearInterval(this._worker_timer);

			this._worker_timer = 0;
			this._camera.release();
			this._camera = null;

			if (this.debug) {
				this.window.destroy();
			}
		}
	}

	_worker() {
		if (!this._camera) {
			return ;
		}

		this._camera.read((err, frame) => {
			if (err) throw err;

			if (frame.width() > 0 && frame.height() > 0) {

				frame.convertGrayscale();
				frame.copyTo(this.filters_frame);

				this.filters_frame.gaussianBlur([3, 3]);
				this.filters_frame.absDiff(this.filters_frame, this.prev_frame);
				this.filters_frame = this.filters_frame.threshold(this.threshold, 255);				

				if (this.debug) {
					this.debug_frame = cv.Matrix.Zeros(FRAME_HEIGHT, FRAME_WIDTH, cv.Constants.CV_BGR2GRAY)
				}

				let contours = this.filters_frame.findContours();
				let countarea = 0;

				for (let i = 0; i < contours.size(); ++i) {
					for (let j = 0; j < contours.cornerCount(i); ++j) {
						let rect = contours.boundingRect(i, j);
						if (rect.width * rect.height > this.contours_area) {
							countarea++;

							if (this.debug) {
								this.debug_frame.rectangle([rect.x, rect.y], [rect.width, rect.height], [255, 255, 255])
							}
						}
					}
				}
				
				if (countarea > 1) {
					this.current_motion_frame_on++;
					this.current_motion_frame_off = 0;			
				} else {
					this.current_motion_frame_on = 0;
					this.current_motion_frame_off++;
				}

				if (this.current_motion_frame_on > this.motion_frame_count_on) {
					this.emit("detect", true);
				} else if (this.current_motion_frame_off > this.motion_frame_count_off) {
					this.emit("detect", false);
				}

				if (this.debug) {
					this.window.show(this.debug_frame);						
					this.window.blockingWaitKey(0, 50);
				}
				
				frame.copyTo(this.prev_frame);
			}
		});
	}

	_convertToBase64(matrix) {

	}
}
	
module.exports = Motion;