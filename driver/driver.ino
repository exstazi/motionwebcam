#define COMMAND_ON_220 1
#define COMMAND_OFF_220 2
#define COMMAND_ON_PLINTH 3
#define COMMAND_OFF_PLINTH 4
#define COMMAND_MANUAL_PLINTH 5
#define COMMAND_ON_RGB 6
#define COMMAND_OFF_RGB 7
#define COMMAND_MANUAL_R 8
#define COMMAND_MANUAL_G 9
#define COMMAND_MANUAL_B 10

#define INC_PWM_PLINTH 1

#define PIN_220_LIGHT 2
#define PIN_PLINTH 3
#define PIN_R_LED 5
#define PIN_G_LED 6
#define PIN_B_LED 9

uint8_t command = 0;
uint8_t data = 0;

uint8_t pwm_plinth = 0;
uint8_t task_plinth = 0;
uint16_t delay_iter_plinth = 100;
long prev_timer_plinth = 0;

uint8_t is_rgb_work = 0;
uint16_t delay_iter_rgb = 100;
long prev_timer_rgb = 0;

void setup() {
  Serial.begin(9600);
  
  pinMode(PIN_220_LIGHT, OUTPUT);
  pinMode(PIN_PLINTH, OUTPUT);
  pinMode(PIN_R_LED, OUTPUT);
  pinMode(PIN_G_LED, OUTPUT);
  pinMode(PIN_B_LED, OUTPUT);
}

void loop() {
  if (Serial.available() > 2) {
    if (Serial.read() == 0xFF) {
      command = Serial.read();
      data = Serial.read();
    }
  }

  switch (command)
  {
    case COMMAND_ON_220:
      digitalWrite(PIN_220_LIGHT, 1);
    break;
    case COMMAND_OFF_220:
      digitalWrite(PIN_220_LIGHT, 0);
    break;
    case COMMAND_ON_PLINTH:
      task_plinth = 255;
      if (data != 0) {
        delay_iter_plinth = data;
      }
    break;
    case COMMAND_OFF_PLINTH:
      task_plinth = 0;
      if (data != 0) {
        delay_iter_plinth = data;
      }
    break;
    case COMMAND_MANUAL_PLINTH:
      pwm_plinth = task_plinth = 0;

      analogWrite(PIN_PLINTH, data);
    break;
    case COMMAND_ON_RGB:
      is_rgb_work = 1;
      if (data != 0) {
        delay_iter_rgb = data;
      }
    break;
    case COMMAND_OFF_RGB:
      is_rgb_work = 0;
      analogWrite(PIN_R_LED, 0);
      analogWrite(PIN_G_LED, 0);
      analogWrite(PIN_B_LED, 0);
    break;
    case COMMAND_MANUAL_R:
      is_rgb_work = 0;
      analogWrite(PIN_R_LED, data);
    break;
    case COMMAND_MANUAL_G:
      is_rgb_work = 0;
      analogWrite(PIN_G_LED, data);
    break;
    case COMMAND_MANUAL_B:
      is_rgb_work = 0;
      analogWrite(PIN_B_LED, data);
    break;
  }
  
  command = 0;

  if (pwm_plinth != task_plinth and (millis() - prev_timer_plinth) > delay_iter_plinth) {
    if (pwm_plinth > task_plinth) {
      pwm_plinth -= INC_PWM_PLINTH; 
    } else {
      pwm_plinth += INC_PWM_PLINTH;
    }
    analogWrite(PIN_PLINTH, pwm_plinth);
    prev_timer_plinth = millis();
  }

  if (is_rgb_work and (millis() - prev_timer_rgb) > delay_iter_rgb) {
    work_rgb();
    prev_timer_rgb = millis();
  }
}

void work_rgb()
{
  static uint16_t i = 0;

  if (i < 256) {
    setFadeColor(PIN_R_LED, PIN_G_LED, PIN_B_LED, i);
  } else if (i < 513) {
    setFadeColor(PIN_G_LED, PIN_B_LED, PIN_R_LED, i - 256);    
  } else if (i < 769) {
    setFadeColor(PIN_B_LED, PIN_R_LED, PIN_G_LED, i - 513);    
  } else {
    i = 0;
    setFadeColor(PIN_R_LED, PIN_G_LED, PIN_B_LED, i);
  }

  i++;
}

void setFadeColor(uint8_t cPin1,uint8_t cPin2,uint8_t cPin3, uint8_t i){  
    analogWrite(cPin1, i);
    analogWrite(cPin2, 255-i);
    analogWrite(cPin3, 255);    
}

